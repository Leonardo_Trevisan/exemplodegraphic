using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

//LEMBRE-SE DESTA USING
using System.Windows.Forms.DataVisualization.Charting;

namespace GraficosBolados
{
    public partial class Form1 : Form
    {
        public Form1(bool graficoA = true)
        {
            InitializeComponent();
            //Existem dois métodos para criar gráficos, escolha com base me um if, por exemplo
            //Depedendo da necessidade. Geralmente pode-se pedir apenas um gráfico, mas talvez
            //O usuário queira escolher entre mais de um
            if (graficoA) load();
            else loadB();
        }

        private void load()
        {
            //Método que gera amostra aleatória de Pessoas com idades e salários
            var amostra = Pessoa.GerarAmostra();

            ///Limpa Séries
            chGraph.Series.Clear();

            //Cria nova Série
            Series s = new Series();
            //Retira-a da legenda
            s.IsVisibleInLegend = false;
            //Define tipo de gráfico como gráfico de barras
            s.ChartType = SeriesChartType.StackedBar;

            //Separá amostra em grupos pela idade
            var grupos = amostra.Select(p => new
            {
                Salario = p.Salario,
                Tipo = p.Idade <= 18 ? "Jovem" :
                    p.Idade <= 30 ? "Adulto" :
                    p.Idade <= 45 ? "Senior" :
                    "Idoso"
            }).GroupBy(e => e.Tipo);

            DataPoint dp = null;

            //Analisa grupos
            foreach (var g in grupos)
            {
                //Cria novo ponto
                dp = new DataPoint();
                //Diz que o texto que deve aparecer em cima da barra é a chave(Jovem, Adulto...)
                dp.Label = g.Key;
                //Diz que o valor é a média do salário do gurpo
                dp.SetValueY(g.Average(e => e.Salario));
                //Define cor com base no tipo(chave do grupo)
                dp.Color = g.Key == "Jovem" ? Color.Red :
                    g.Key == "Adulto" ? Color.Blue :
                    g.Key == "Senior" ? Color.LawnGreen :
                    Color.LightBlue;
                //Adiciona ponto na série
                s.Points.Add(dp);
            }
            //Adiciona Série ao gráfico
            chGraph.Series.Add(s);

            //Limpa legendas
            chGraph.Legends.Clear();
            //Adiciona novas legendas a um objeto Legend
            Legend l = new Legend("Legenda");
            l.CustomItems.Add(new LegendItem()
            {
                Name = "Jovem",
                Color = Color.Red
            });
            l.CustomItems.Add(new LegendItem()
            {
                Name = "Aduto",
                Color = Color.Blue
            });
            l.CustomItems.Add(new LegendItem()
            {
                Name = "Senior",
                Color = Color.LawnGreen
            });
            l.CustomItems.Add(new LegendItem()
            {
                Name = "Idosos",
                Color = Color.LightBlue
            });
            //Adiciona Lengend ao Gráfico
            chGraph.Legends.Add(l);

            //Adiciona um título ao gráfico
            chGraph.Titles.Add("Salário Médio por faixa etária");
        }

        /// <summary>
        /// Exemplo analogo ao que está em cima. Porém, o de cima separá as pessoas por faixa etária
        /// e diz o salário médio desta. Está mostra a soma dos salários por faixa etária em um
        /// gráfico de pizza
        /// </summary>
        private void loadB()
        {

            var amostra = Pessoa.GerarAmostra();

            chGraph.Series.Clear();
            Series s = new Series();
            s.IsVisibleInLegend = false;
            s.ChartType = SeriesChartType.Pie; //Define gráfico de pizza

            var grupos = amostra.Select(p => new
            {
                Salario = p.Salario,
                Tipo = p.Idade <= 18 ? "Jovem" :
                    p.Idade <= 30 ? "Adulto" :
                    p.Idade <= 45 ? "Senior" :
                    "Idoso"
            }).GroupBy(e => e.Tipo);

            DataPoint dp = null;

            foreach (var g in grupos)
            {
                dp = new DataPoint();
                dp.Label = g.Key;
                dp.SetValueY(g.Sum(e => e.Salario)); //Seta valor como a soma dos salários
                dp.Color = g.Key == "Jovem" ? Color.Red :
                    g.Key == "Adulto" ? Color.Blue :
                    g.Key == "Senior" ? Color.LawnGreen :
                    Color.LightBlue;
                s.Points.Add(dp);
            }

            chGraph.Legends.Clear();
            Legend l = new Legend("Legenda");
            l.CustomItems.Add(new LegendItem()
            {
                Name = "Jovem",
                Color = Color.Red
            });
            l.CustomItems.Add(new LegendItem()
            {
                Name = "Aduto",
                Color = Color.Blue
            });
            l.CustomItems.Add(new LegendItem()
            {
                Name = "Senior",
                Color = Color.LawnGreen
            });
            l.CustomItems.Add(new LegendItem()
            {
                Name = "Idosos",
                Color = Color.LightBlue
            });
            chGraph.Legends.Add(l);

            chGraph.Titles.Add("Salário total de cada faixa etária");

            chGraph.Series.Add(s);
        }
    }

    //Classe base para os dados
    public class Pessoa
    {
        public Pessoa(int idade, decimal salario)
        {
            this.Idade = idade;
            this.Salario = salario;
        }

        public int Idade { get; set; }
        public decimal Salario { get; set; }

        public static List<Pessoa> GerarAmostra()
        {
            Random rand = new Random(DateTime.Now.Millisecond * DateTime.Now.Second);
            List<Pessoa> result = new List<Pessoa>();
            int total = rand.Next(10, 100);
            int idade = 0;
            for (int i = 0; i < total; i++)
            {
                idade = rand.Next(16, 60);
                result.Add(new Pessoa(idade,
                    (decimal)(rand.NextDouble() * rand.Next(900, 12000) * (1d - 1d / idade))));
            }
            return result;
        }
    }
}
